<?php

namespace webmonkey\defcode\controllers;

use webmonkey\defcode\DefCode;
use webmonkey\defcode\models\Phone;
use webmonkey\defcode\models\Operator;
use webmonkey\defcode\models\Region;
use yii\base\Exception;
use yii\base\ExitException;
use Yii;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

/**
 * Default controller for the `defCode` module
 * @property string $path
 * @property boolean $isRelevance
 * @property DefCode $module
 */
class DefCodeController extends Controller
{

    public function init()
    {
        parent::init();
        $this->module = Yii::$app->getModule('defcode');
    }

    public function actionImport()
    {
        if (!$this->isRelevance) {
            $this->downloadFile();
        }

        $operators = Operator::find()->indexBy('name')->select('id')->column();
        $regions = Region::find()->indexBy('name')->select('id')->column();

        $transaction = Yii::$app->db->beginTransaction();

        try {
            if (($handle = fopen($this->path, "r")) !== FALSE) {

                while (($data = fgetcsv($handle, 255, ";")) !== FALSE) {

                    $def = intval(ArrayHelper::getValue($data, 0));

                    if(empty($def)) {
                        continue;
                    }
                    $start = intval($this::getColValue($data, 1));
                    $end = intval($this::getColValue($data, 2));
                    $operator = trim($this::getColValue($data, 4));
                    $region = trim($this::getColValue($data, 5));

                    $code = Phone::find()
                        ->where(['def' => $def])
                        ->andWhere(['start' => $start])
                        ->andWhere(['end' => $end])
                        ->one();

                    if($code === null) {
                        $code = new Phone();
                        $code->def = $def;
                        $code->start = $start;
                        $code->end = $end;
                    }

                    if(key_exists($operator, $operators)) {
                        $code->operator_id = $operators[$operator];
                    } else {
                        $newOperator = new Operator();
                        $newOperator->name = $operator;

                        if($newOperator->save()) {
                            $operators[$operator] = $newOperator->getPrimaryKey();
                            $code->operator_id = $operators[$operator];
                        }
                    }

                    if(key_exists($region, $regions)) {
                        $code->region_id = $regions[$region];
                    } else {
                        $newOperator = new Region();
                        $newOperator->name = $region;

                        if($newOperator->save()) {
                            $regions[$region] = $newOperator->getPrimaryKey();
                            $code->region_id = $regions[$region];
                        }
                    }

                    $code->save();
                }
                $transaction->commit();
                fclose($handle);
            }

        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }

    }

    protected function downloadFile()
    {
        try {
            $ch = curl_init($this->module->url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $data = curl_exec($ch);
            curl_close($ch);

            file_put_contents($this->path, $data);
        } catch (Exception $e) {
            throw new ExitException(500, 'File download error');
        }
    }

    /**
     * @return bool
     */
    protected function getIsRelevance()
    {
        $path = $this->path;

        if (file_exists($path)) {
            $lastUpdate = filemtime($path);

            return strtotime('+' . $this->module->cacheLimit . 'day', $lastUpdate) >= time();
        }
        return false;
    }

    /**
     * @return string
     */
    protected function getPath()
    {
        return $this->module->filePath . $this->module->fileName;
    }

    protected function getColValue($row, $index, $defaultValue = '')
    {
        return mb_convert_encoding(trim(ArrayHelper::getValue($row, $index, $defaultValue)), "UTF-8", 'CP1251');
    }
}
