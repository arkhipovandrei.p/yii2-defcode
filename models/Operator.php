<?php

namespace webmonkey\defcode\models;

use Yii;

/**
 * This is the model class for table "{{%def_code_operator}}".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Code[] $defCodes
 */
class Operator extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%def_code_operator}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefCodes()
    {
        return $this->hasMany(Code::className(), ['operator_id' => 'id']);
    }
}
