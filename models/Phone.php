<?php

namespace webmonkey\defcode\models;

use Yii;

/**
 * This is the model class for table "{{%def_code}}".
 *
 * @property integer $id
 * @property integer $def
 * @property integer $start
 * @property integer $end
 * @property integer $operator_id
 * @property integer $region_id
 *
 * @property Operator $operator
 * @property Region $region
 * @property integer $regionId
 */
class Phone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%def_code}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['def', 'start', 'end', 'operator_id', 'region_id'], 'integer'],
            [['operator_id'], 'exist', 'skipOnError' => true, 'targetClass' => Operator::className(), 'targetAttribute' => ['operator_id' => 'id']],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'def' => 'Def',
            'start' => 'Start',
            'end' => 'End',
            'operator_id' => 'Operator ID',
            'region_id' => 'Region ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperator()
    {
        return $this->hasOne(Operator::className(), ['id' => 'operator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    public static function findRegion($phone, $onlyName = false)
    {
        $phone = preg_replace('/[^0-9]/','', $phone);

        if (!preg_match('/^\d{11}$/', $phone)) {
            return 'Неверный номер телефона';
        }

        $def = intval(substr($phone,1, 3));
        $number = substr($phone,4, 10);

        $id = self::find()
            ->select('region_id')
            ->where(['def' => $def])
            ->andWhere(['<=', 'start', $number])
            ->andWhere(['>=', 'end', $number])
            ->one();

        if(empty($id)) {
            return 'Регион не найден';
        }

        if($onlyName) {
            return Region::find()
                ->where(['id' => $id])
                ->select('name')
                ->scalar();
        }

        return Region::findOne($id);
    }

    public static function getRegionId($phone)
    {
        $region = self::findRegion($phone);
        return (!empty($region) && $region instanceof Region) ? $region->id : null;
    }
}
