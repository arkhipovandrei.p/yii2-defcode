<?php

namespace webmonkey\defcode\models;

use Yii;

/**
 * This is the model class for table "{{%def_code_region}}".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Code[] $defCodes
 */
class Region extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%def_code_region}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefCodes()
    {
        return $this->hasMany(Code::className(), ['region_id' => 'id']);
    }
}
