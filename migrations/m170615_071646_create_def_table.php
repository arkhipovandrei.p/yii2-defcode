<?php

use yii\db\Migration;

/**
 * Handles the creation of table `def_region`.
 */
class m170615_071646_create_def_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        $this->createTable('{{%def_code_region}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);

        $this->createTable('{{%def_code_operator}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);

        $this->createTable('{{%def_code}}', [

            'id' => $this->primaryKey(),
            'def' => $this->integer(),
            'start' => $this->integer(),
            'end' => $this->integer(),
            'operator_id' => $this->integer(),
            'region_id' => $this->integer()

        ]);

        $this->addForeignKey('fk_def_code_region', '{{%def_code}}', 'region_id', '{{%def_code_region}}', 'id', 'SET NULL', 'cascade');
        $this->addForeignKey('fk_def_code_operator', '{{%def_code}}', 'operator_id', '{{%def_code_operator}}', 'id', 'SET NULL', 'cascade');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_def_code_region', '{{%def_code}}');
        $this->dropForeignKey('fk_def_code_operator', '{{%def_code}}');

        $this->dropTable('{{%def_code_region}}');
        $this->dropTable('{{%def_code_operator}}');
        $this->dropTable('{{%def_code}}');
    }
}
