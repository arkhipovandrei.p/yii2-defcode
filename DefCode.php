<?php

namespace webmonkey\defcode;

use Yii;
/**
 * defCode module definition class
 */
class DefCode extends \yii\base\Module
{
    public $filePath = '@runtime/';
    public $fileName = 'data.csv';
    public $url = 'https://rossvyaz.gov.ru/docs/articles/Kody_DEF-9kh.csv';
    public $cacheLimit = 120;
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\defCode\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->filePath = Yii::getAlias($this->filePath);

        if (!is_dir($this->filePath)) {
            mkdir($this->filePath);
        }
        // custom initialization code goes here
    }



}
