Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```sh
php composer.phar require webmonkey/defcode 
```

or add

```json
"webmonkey/defcode": "*"
```

to the require section of your composer.json.


Usage
-----

Migrations
```
php console/yii migrate/up --migrationPath=@vendor/webmonkey/defcode/migrations
```
Configure the component in your configuration file (web.php).
 
common config.php
```php
'modules'=>[
    ...
    'defcode' => [
        'class' => 'webmonkey\defcode\DefCode',
    ],
    ...
],
```
 
console config.php
```php
'controllerMap'=>[
    ...
    'def-code' => [
        'class' => 'webmonkey\defcode\controllers\UpdateController'
    ],
    ...
],
```
 